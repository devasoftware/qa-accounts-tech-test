# Marcus Bond Pre Assessment - Accounts REST API

## Overview
The Accounts REST API application is implemented as a Spring Boot application that by default runs on port 8080.

## Prerequisites
In order to run the application or run the tests a Java Runtime at version 8 or greater is required. The application has been tested using Java 8 on Mac.


## Build the application
Maven is used to build the application, however due to the use of the Maven Wrapper there is no prerequisite to install Maven.

Open a terminal at directory qa-accounts-rest
### Mac or Linux
./mvnw clean install

### On Windows
mvnw clean install

## Run the application
The application is configured with the Spring Boot Maven plugin and can be run from the command line as follows:

Open a terminal at directory qa-accounts-rest
### Mac or Linux
./mvnw spring-boot:run

### On Windows
mvnw spring-boot:run
