package io.marcusbond.qa.accounts.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import io.marcusbond.qa.accounts.entity.Account;
import io.marcusbond.qa.accounts.repository.AccountRepository;

/**
 * Unit tests for {@link AccountServiceImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTests {

	private AccountServiceImpl accountService;

	@Mock
	private AccountRepository accountRepository;

	@Captor
	private ArgumentCaptor<Account> accountCaptor;

	@Before
	public void setup() {
		accountService = new AccountServiceImpl(accountRepository);
	}

	private void noAccounts() {
		when(accountRepository.findAll()).thenReturn(new ArrayList<>());
	}

	private void initialAccountsList() {
		List<Account> accountsList = new ArrayList<>();
		accountsList.add(new Account(1L, "1234", "John", "Doe"));
		accountsList.add(new Account(2L, "1235", "Jane", "Doe"));
		accountsList.add(new Account(3L, "1236", "Jim", "Taylor"));
		when(accountRepository.findAll()).thenReturn(accountsList);
	}

	@Test
	public void testEmptyGetAllAccounts() {
		noAccounts();
		List<Account> accounts = accountService.getAllAccounts();
		assertThat(accounts).isNotNull();
		assertThat(accounts).isEmpty();
	}

	@Test
	public void testGetAllAccounts() {
		initialAccountsList();
		List<Account> accounts = accountService.getAllAccounts();
		assertThat(accounts).isNotNull();
		assertThat(accounts).hasSize(3);
	}

	@Test
	public void testCreateAccount() {
		when(accountRepository.save(accountCaptor.capture())).then(invocation -> {
			Account created = accountCaptor.getValue();
			created.setId(2L);
			return created;
		});

		Account account = new Account("1111", "test-firstName", "test-secondName");
		Account created = accountService.createAccount(account);
		assertThat(created).isNotNull();
		assertThat(created.getId()).isEqualTo(2L);
	}

	@Test
	public void testCreateAccountWhenAccountNumberAlreadyExists() {
		when(accountRepository.save(Mockito.any())).thenThrow(new DataIntegrityViolationException("Constraint violation"));

		try {
			accountService.createAccount(new Account("1111", "test-firstName2", "test-secondName2"));
			fail("Expected an AccountAlreadyExists exception");
		} catch (AccountAlreadyExistsException e) {
			assertThat(e.getMessage()).isEqualTo("account 1111 already exists");
		}
	}

	@Test
	public void testDeleteAccount() {
		Account account = mock(Account.class);
		when(accountRepository.findById(1L)).thenReturn(Optional.of(account));
		
		Account deleted = accountService.deleteAccount(1);
		assertThat(deleted).isNotNull();
		assertThat(deleted).isSameAs(account);
	}

	@Test
	public void testDeleteAccountWhenAccountDoesNotExist() {
		when(accountRepository.findById(2L)).thenReturn(Optional.empty());
		try {
			accountService.deleteAccount(2);
			fail("Expected an AccountNotFoundException exception");
		} catch (AccountNotFoundException e) {
			assertThat(e.getMessage()).isEqualTo("account 2 not found");
		}
	}
}
