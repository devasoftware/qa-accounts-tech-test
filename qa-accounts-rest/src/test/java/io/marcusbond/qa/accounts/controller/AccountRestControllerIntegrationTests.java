package io.marcusbond.qa.accounts.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import io.marcusbond.qa.accounts.entity.Account;
import io.marcusbond.qa.accounts.repository.AccountRepository;

/**
 * End to end integration tests over HTTP.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AccountRestControllerIntegrationTests {

	private static final String ACCOUNT_RESOURCE_PATH = "/rest/account/json";
	private static final String ACCOUNT_RESOURCE_PATH_WITH_ID = "/rest/account/json/{id}";

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private AccountRepository accountRepository;

	@Before
	public void clearData() {
		accountRepository.deleteAll();
	}

	private void initialAccountsList() {
		accountRepository.save(new Account("1234", "John", "Doe"));
		accountRepository.save(new Account("1235", "Jane", "Doe"));
		accountRepository.save(new Account("1236", "Jim", "Taylor"));
	}

	private void accountAlreadyExists() {
		accountRepository.save(new Account("8888", "James", "Dean"));

	}

	@Test
	public void givenNoAccounts_whenGetAllAccounts_responseIsEmptyArray() {
		ResponseEntity<AccountDto[]> response = restTemplate.getForEntity(ACCOUNT_RESOURCE_PATH, AccountDto[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();
		assertThat(response.getBody().length).isEqualTo(0);

	}

	@Test
	public void givenInitialAccountsList_whenGetAllAccounts_responseIsArrayOfAccounts() {
		initialAccountsList();

		ResponseEntity<AccountDto[]> allAccounts = restTemplate.getForEntity(ACCOUNT_RESOURCE_PATH, AccountDto[].class);
		assertThat(allAccounts.getBody().length).isEqualTo(3);
	}

	@Test
	public void givenAccountDoesNotExist_whenCreateAccount_thenExpect200AndSuccessMessage() throws Exception {
		initialAccountsList();
		assertThat(accountRepository.count()).isEqualTo(3);

		Account account = new Account("8888", "James", "Dean");
		ResponseEntity<ResponseMessageDto> newEntryResponse = restTemplate.postForEntity(ACCOUNT_RESOURCE_PATH, account,
				ResponseMessageDto.class);
		assertThat(newEntryResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(newEntryResponse.getBody()).isNotNull();
		assertThat(newEntryResponse.getBody().getMessage()).isEqualTo("account has been successfully added");

		assertThat(accountRepository.count()).isEqualTo(4);
	}

	@Test
	public void givenAccountExists_whenCreateAccount_thenExpect400AndErrorMessage() throws Exception {
		initialAccountsList();
		accountAlreadyExists();
		assertThat(accountRepository.count()).isEqualTo(4);

		AccountDto account = new AccountDto("8888", "James", "Dean");

		ResponseEntity<String> response = restTemplate.postForEntity(ACCOUNT_RESOURCE_PATH, account, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		String responseJson = response.getBody();
		JSONAssert.assertEquals("{ \"message\" : \"account 8888 already exists\"}", responseJson,
				JSONCompareMode.STRICT);

		assertThat(accountRepository.count()).isEqualTo(4);
	}
	
	@Test
	public void givenInitialAccountsList_whenAccountDeleted_responseHasMessageAndAccountIsDeleted() {
		initialAccountsList();

		// Get an account to delete
		Account accountToDelete = accountRepository.findAll().iterator().next();

		ResponseEntity<ResponseMessageDto> deletionResponse = restTemplate.exchange(ACCOUNT_RESOURCE_PATH_WITH_ID, HttpMethod.DELETE, null, ResponseMessageDto.class, accountToDelete.getId());
		assertThat(deletionResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(deletionResponse.getBody()).isNotNull();
		assertThat(deletionResponse.getBody().getMessage()).isEqualTo("account successfully deleted");

		Optional<Account> accountById = accountRepository.findById(accountToDelete.getId());
		assertThat(accountById).isEmpty();
	}


}
