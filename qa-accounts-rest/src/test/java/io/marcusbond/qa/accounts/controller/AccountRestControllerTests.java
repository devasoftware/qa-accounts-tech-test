package io.marcusbond.qa.accounts.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.marcusbond.qa.accounts.entity.Account;
import io.marcusbond.qa.accounts.service.AccountAlreadyExistsException;
import io.marcusbond.qa.accounts.service.AccountNotFoundException;
import io.marcusbond.qa.accounts.service.AccountService;

/**
 * Unit tests for the AccountRestController.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountRestController.class)
public class AccountRestControllerTests {

	private static final String ACCOUNT_RESOURCE_PATH = "/rest/account/json";
	private static final String ACCOUNT_RESOURCE_PATH_WITH_ID = "/rest/account/json/{id}";

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private AccountService accountService;

	private void noAccounts() {
		when(accountService.getAllAccounts()).thenReturn(new ArrayList<>());
	}

	private void initialAccountsList() {
		List<Account> accountsList = new ArrayList<>();
		accountsList.add(new Account(1L, "1234", "John", "Doe"));
		accountsList.add(new Account(2L, "1235", "Jane", "Doe"));
		accountsList.add(new Account(3L, "1236", "Jim", "Taylor"));
		when(accountService.getAllAccounts()).thenReturn(accountsList);
	}

	private void accountAlreadyExists() {
		when(accountService.createAccount(Mockito.any())).thenThrow(new AccountAlreadyExistsException("8888"));
	}

	@Test
	public void givenNoAccounts_whenGetAllAccounts_thenExpect200AndEmptyArray()
			throws JsonProcessingException, Exception {
		noAccounts();
		mvc.perform(get(ACCOUNT_RESOURCE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$").isEmpty());
	}

	@Test
	public void givenInitialAccountsList_whenGetAllAccounts_thenExpect200AndAccountsArray()
			throws JsonProcessingException, Exception {
		initialAccountsList();
		mvc.perform(get(ACCOUNT_RESOURCE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$[0].id").value(1)).andExpect(jsonPath("$[0].accountNumber").value("1234"))
				.andExpect(jsonPath("$[0].firstName").value("John")).andExpect(jsonPath("$[0].secondName").value("Doe"))
				.andExpect(jsonPath("$[1].accountNumber").value("1235")).andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].firstName").value("Jane")).andExpect(jsonPath("$[1].secondName").value("Doe"))
				.andExpect(jsonPath("$[2].accountNumber").value("1236")).andExpect(jsonPath("$[2].id").value(3))
				.andExpect(jsonPath("$[2].firstName").value("Jim"))
				.andExpect(jsonPath("$[2].secondName").value("Taylor"));
	}

	@Test
	public void givenAccountDoesNotExist_whenCreateAccount_thenExpect200AndSuccessMessage() throws Exception {
		Account account = new Account("8888", "James", "Dean");
		mvc.perform(post(ACCOUNT_RESOURCE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(account))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("account has been successfully added"));

	}

	@Test
	public void givenAccountExists_whenCreateAccount_thenExpect400AndErrorMessage() throws Exception {
		accountAlreadyExists();

		Account account = new Account("8888", "James", "Dean");
		mvc.perform(post(ACCOUNT_RESOURCE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(account))).andExpect(status().isBadRequest())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("account 8888 already exists"));

	}
	
	@Test
	public void givenAccountExists_whenAccountDeleted_thenExpect200AndMessage() throws JsonProcessingException, Exception {
		Account account = mock(Account.class);
		when(accountService.deleteAccount(1L)).thenReturn(account);

		mvc.perform(delete(ACCOUNT_RESOURCE_PATH_WITH_ID, 1L).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("account successfully deleted"));
	}

	@Test
	public void givenAccountDoesNotExist_whenAccountDeleted_thenExpect404AndErrorMessage() throws JsonProcessingException, Exception {
		when(accountService.deleteAccount(2L)).thenThrow(new AccountNotFoundException(2));

		mvc.perform(delete(ACCOUNT_RESOURCE_PATH_WITH_ID, 2L).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isNotFound())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("account 2 not found"));
	}

}
