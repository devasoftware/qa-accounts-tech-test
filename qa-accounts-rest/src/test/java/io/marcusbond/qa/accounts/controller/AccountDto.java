package io.marcusbond.qa.accounts.controller;

/**
 * Rest API Account DTO for testing.
 *
 */
public class AccountDto {

	private Long id;
	private String accountNumber;
	private String firstName;
	private String secondName;

	public AccountDto() {
		super();
	}

	public AccountDto(String accountNumber, String firstName, String secondName) {
		super();
		this.accountNumber = accountNumber;
		this.firstName = firstName;
		this.secondName = secondName;
	}

	public AccountDto(Long id, String accountNumber, String firstName, String secondName) {
		super();
		this.id = id;
		this.accountNumber = accountNumber;
		this.firstName = firstName;
		this.secondName = secondName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

}
