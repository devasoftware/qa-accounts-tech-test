package io.marcusbond.qa.accounts;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import io.marcusbond.qa.accounts.entity.Account;
import io.marcusbond.qa.accounts.repository.AccountRepository;

/**
 * Tests for {@link AccountRepository} where entity configuration requires
 * checking.
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * Test the unique account number constraint.
	 */
	@Test
	public void testAccountNumberConstraint() {
		accountRepository.save(new Account("1234", "first1", "second1"));
		try {
			accountRepository.save(new Account("1234", "first2", "second2"));
			entityManager.flush();
		} catch (PersistenceException e) {
			assertThat(e.getCause()).isInstanceOf(ConstraintViolationException.class);
		}
	}
}
