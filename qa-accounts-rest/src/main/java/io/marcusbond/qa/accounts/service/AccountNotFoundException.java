package io.marcusbond.qa.accounts.service;

public class AccountNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3113488521242432190L;

	public AccountNotFoundException(long id) {
		super(String.format("account %s not found", id));
	}

}
