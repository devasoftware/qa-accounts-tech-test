package io.marcusbond.qa.accounts.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Account model POJO. This class acts as both a Rest API DTO and as the
 * persisted entity.
 *
 */
@Entity
@Table(name = "ACCOUNT")
public class Account {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	private String accountNumber;

	private String firstName;
	private String secondName;

	/**
	 * No-arg constructor for use by persistence and REST frameworks.
	 */
	public Account() {
		super();
	}

	/**
	 * Convenience constructor for creating instances with all fields that are
	 * controlled by client code.
	 * 
	 * @param accountNumber
	 *            the account number, this must be unique across all Account
	 *            entities.
	 * @param firstName
	 *            the first name of the account owner.
	 * @param secondName
	 *            the second name of the account owner.
	 */
	public Account(String accountNumber, String firstName, String secondName) {
		super();
		this.accountNumber = accountNumber;
		this.firstName = firstName;
		this.secondName = secondName;
	}

	/**
	 * All args constructor.
	 * 
	 * @param id
	 *            the database id.
	 * @param accountNumber
	 *            the account number, this must be unique across all Account
	 *            entities.
	 * @param firstName
	 *            the first name of the account owner.
	 * @param secondName
	 *            the second name of the account owner.
	 */
	public Account(Long id, String accountNumber, String firstName, String secondName) {
		super();
		this.id = id;
		this.accountNumber = accountNumber;
		this.firstName = firstName;
		this.secondName = secondName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

}
