package io.marcusbond.qa.accounts.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.marcusbond.qa.accounts.entity.Account;
import io.marcusbond.qa.accounts.service.AccountAlreadyExistsException;
import io.marcusbond.qa.accounts.service.AccountNotFoundException;
import io.marcusbond.qa.accounts.service.AccountService;

/**
 * Provides the Accounts REST resource.
 *
 */
@RestController
public class AccountRestController {

	private final AccountService accountService;

	/**
	 * Constructor for all dependencies.
	 * 
	 * @param accountService
	 *            the account service.
	 */
	public AccountRestController(AccountService accountService) {
		super();
		this.accountService = accountService;
	}

	/**
	 * Provides the list of all available accounts.
	 * 
	 * @return
	 */
	@GetMapping("/rest/account/json")
	public List<Account> getAllAccounts() {
		return accountService.getAllAccounts();
	}

	@PostMapping("/rest/account/json")
	public ResponseEntity<ResponseMessage> createAccount(@RequestBody Account account) {
		try {
			accountService.createAccount(account);
			return ResponseEntity.ok(new ResponseMessage("account has been successfully added"));
		} catch (AccountAlreadyExistsException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(e.getMessage()));
		}
	}

	@DeleteMapping("/rest/account/json/{id}")
	public ResponseEntity<ResponseMessage> deleteAccount(@PathVariable("id") long id) {
		try {
			accountService.deleteAccount(id);
			return ResponseEntity.ok(new ResponseMessage("account successfully deleted"));
		} catch (AccountNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(e.getMessage()));
		}
	}
}
