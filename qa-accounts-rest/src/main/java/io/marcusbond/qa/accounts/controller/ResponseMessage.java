package io.marcusbond.qa.accounts.controller;

/**
 * Simple DTO that provides the response message.
 */
public class ResponseMessage {

	private final String message;

	/**
	 * Constructor with message to return to the client.
	 * 
	 * @param message
	 *            the message.
	 */
	public ResponseMessage(String message) {
		super();
		this.message = message;
	}

	/**
	 * Getter for message.
	 * 
	 * @return the messsage.
	 */
	public String getMessage() {
		return message;
	}
}
