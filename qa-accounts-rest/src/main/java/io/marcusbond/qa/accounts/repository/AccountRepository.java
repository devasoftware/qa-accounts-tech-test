package io.marcusbond.qa.accounts.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.marcusbond.qa.accounts.entity.Account;

/**
 * JPA repository for {@link Account}.
 *
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

}
