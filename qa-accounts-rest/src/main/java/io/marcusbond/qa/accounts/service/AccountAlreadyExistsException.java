package io.marcusbond.qa.accounts.service;

/**
 * Exception class to denote that an Account already exists having the same
 * account number.
 *
 */
public class AccountAlreadyExistsException extends RuntimeException {
	private static final long serialVersionUID = -3311870673590266463L;

	public AccountAlreadyExistsException(String accountNumber) {
		super(String.format("account %s already exists", accountNumber));
	}

	
}
