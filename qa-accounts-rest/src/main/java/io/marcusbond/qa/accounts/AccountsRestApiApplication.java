package io.marcusbond.qa.accounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.marcusbond.qa.accounts.repository.AccountRepository;
import io.marcusbond.qa.accounts.service.AccountService;
import io.marcusbond.qa.accounts.service.AccountServiceImpl;

@SpringBootApplication
public class AccountsRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountsRestApiApplication.class, args);
	}

	@Bean
	public AccountService accountService(AccountRepository accountRepository) {
		return new AccountServiceImpl(accountRepository);
	}
}
