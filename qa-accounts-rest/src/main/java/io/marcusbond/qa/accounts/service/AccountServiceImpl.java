package io.marcusbond.qa.accounts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.DataIntegrityViolationException;

import io.marcusbond.qa.accounts.entity.Account;
import io.marcusbond.qa.accounts.repository.AccountRepository;

/**
 * Implementation of the {@link AccountService}.
 */
public class AccountServiceImpl implements AccountService {

	private final AccountRepository accountRepository;

	/**
	 * Constructor with all required dependencies.
	 * 
	 * @param accountRepository
	 *            the account repository.
	 */
	public AccountServiceImpl(AccountRepository accountRepository) {
		super();
		this.accountRepository = accountRepository;
	}

	@Override
	public List<Account> getAllAccounts() {
		Iterable<Account> repoEntries = accountRepository.findAll();
		List<Account> accountList = new ArrayList<>();
		repoEntries.iterator().forEachRemaining(accountList::add);
		return accountList;
	}

	@Override
	public Account createAccount(Account account) {
		try {
			return accountRepository.save(account);
		} catch (DataIntegrityViolationException e) {
			throw new AccountAlreadyExistsException(account.getAccountNumber());
		}
	}

	@Override
	public Account deleteAccount(long id) {
		Optional<Account> account = accountRepository.findById(id);
		if (account.isPresent()) {
			Account deleted = account.get();
			accountRepository.delete(deleted);
			return deleted;
		}
		throw new AccountNotFoundException(id);
	}

}
