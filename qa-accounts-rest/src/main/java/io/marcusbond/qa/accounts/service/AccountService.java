package io.marcusbond.qa.accounts.service;

import java.util.List;

import io.marcusbond.qa.accounts.entity.Account;

/**
 * Business logic service interface for Accounts
 *
 */
public interface AccountService {

	/**
	 * Returns all of the {@link Account}s in the system.
	 * 
	 * @return a List of all accounts.
	 */
	public List<Account> getAllAccounts();

	/**
	 * Creates a new Account.
	 * <p>
	 * If an account already exists having the same
	 * {@link Account#getAccountNumber()} then an
	 * {@link AccountAlreadyExistsException} is thrown.
	 * 
	 * @param account
	 *            the account to create.
	 * @return the created account.
	 */
	public Account createAccount(Account account);

	/**
	 * Deletes the Account.
	 * <p>
	 * If an account having the provided Id does not exists then a
	 * {@link AccountNotFoundException} exception is thrown.
	 * 
	 * @param id
	 *            the Id of the account to be deleted.
	 * @return the deleted account.
	 */
	public Account deleteAccount(long id);

}
